// Create a data analytics app
// Parse different input sources and their values
// Store the metrics in a columnar format
// Provide an API to query and aggregate data

import java.util.*;
import java.io.*;

public class DataAnalyticsApp {
    private Map<String, List<Double>> metrics;
    
    public DataAnalyticsApp() {
        metrics = new HashMap<>();
    }
    
    public void parseInput(String source, String value) {
        try {
            double metricValue = Double.parseDouble(value);
            metrics.computeIfAbsent(source, k -> new ArrayList<>()).add(metricValue);
        } catch (NumberFormatException e) {
            System.err.println("Invalid metric value: " + value);
        }
    }
    
    public void storeMetrics(String filename) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            for (Map.Entry<String, List<Double>> entry : metrics.entrySet()) {
                writer.write(entry.getKey() + "," + String.join(",", entry.getValue().stream().map(Object::toString).toArray(String[]::new)));
                writer.newLine();
            }
        }
    }
    
    public double queryMetric(String source, String aggregation) {
        List<Double> values = metrics.get(source);
        if (values == null || values.isEmpty()) {
            return 0.0;
        }
        
        switch (aggregation.toLowerCase()) {
            case "sum":
                return values.stream().mapToDouble(Double::doubleValue).sum();
            case "avg":
                return values.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
            case "max":
                return values.stream().mapToDouble(Double::doubleValue).max().orElse(0.0);
            case "min":
                return values.stream().mapToDouble(Double::doubleValue).min().orElse(0.0);
            default:
                throw new IllegalArgumentException("Unsupported aggregation: " + aggregation);
        }
    }
    
    public static void main(String[] args) {
        DataAnalyticsApp app = new DataAnalyticsApp();
        
        // Example usage
        app.parseInput("temperature", "25.5");
        app.parseInput("temperature", "26.2");
        app.parseInput("humidity", "60.0");
        
        try {
            app.storeMetrics("metrics.csv");
            System.out.println("Metrics stored successfully.");
        } catch (IOException e) {
            System.err.println("Error storing metrics: " + e.getMessage());
        }
        
        System.out.println("Average temperature: " + app.queryMetric("temperature", "avg"));
        System.out.println("Max humidity: " + app.queryMetric("humidity", "max"));
    }
}

