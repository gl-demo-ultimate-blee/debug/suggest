# write me a python app that converts watt hours to amp hours
# it should ask for either 12v or 120v

voltage = int(input("Enter the voltage (12V or 120V): "))

if voltage == 12:
    print("Calculating watt-hours to amp-hours for 12V system...")
    watt_hours = float(input("Enter the watt-hours: "))
    amp_hours = watt_hours / 12
    print(f"{watt_hours} watt-hours is equal to {amp_hours:.2f} amp-hours.")
elif voltage == 120:
    print("Calculating watt-hours to amp-hours for 120V system...")
    watt_hours = float(input("Enter the watt-hours: "))
    amp_hours = watt_hours / 120
    print(f"{watt_hours} watt-hours is equal to {amp_hours:.2f} amp-hours.")
else:
    print("Invalid voltage. Please enter 12V or 120V.")

