# this function converts yards to meters
def yards_to_meters(yards):
    return yards * 0.9144


# this function converts meters to yards
def meters_to_yards(meters):
    return meters / 0.9144


# this function converts feet to meters
def feet_to_meters(feet):
    return feet * 0.3048


# this function converts meters to feet
def meters_to_feet(meters):
    return meters / 0.3048


# this function converts miles to kilometers
def miles_to_kilometers(miles):
    return miles * 1.60934


# this function converts inches to cm
def inches_to_cm(inches):
    return inches * 2.54


# this function converts cm to inches
def cm_to_inches(cm):
    return cm / 2.54


# this function converts kilometers to miles
def kilometers_to_miles(kilometers):
    return kilometers / 1.60934
